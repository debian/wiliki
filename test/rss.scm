;; test wiliki.rss

(use gauche.test)
(use wiliki)

(test-start "rss")
(use wiliki.rss)
(test-module 'wiliki.rss)

;; more tests to come...

(test-end)
